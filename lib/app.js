
global.config = require('../config');

const explorerAPI = require('./explorerapi');

async function initTransactions() {

    let types = [254, 3, 2, 0], page = 1, address = poolOwner;
    let type = types.pop();

    console.log('init transactions');

    while (true) {

        let txns = await explorerAPI.getTransactions(address, type, page);

        if (!txns) txns = [];
        let isneedBreak = false;
        if (txns.length > 0 && (type == 0 || type == 254)) {
            let tx = txns[0];
            if (tx.data && tx.data.blockHeight < config.startHeight) isneedBreak = true;
        }

        if (txns.length == 0 || isneedBreak) {

            if (types.length == 0) break;

            type = types.pop();

            if (type == 254 && page == 1) await updateWorkers();

            page = 1;
            continue;
        }
        let theend = false;
        if (txns.length > 0) {
            let tx = txns[0];

            let instance = await Transaction.find({ type: tx.type, amount: tx.amount, time: tx.time, blockHash: tx.blockHash });

            if (instance.length > 0) theend = true;
        }
        await saveTransactions(txns);

        if (theend) {
            console.log(`type=${type} in database has been synced`);
            if (types.length == 0) break;
            type = types.pop();
            if (type == 254 && page == 1) await updateWorkers();
            page = 1;
            continue;
        }

        console.log(`init type:${type} page:${page}`)
        page++;
    }

    setTimeout(initTransactions, 1000 * 30);
}

async function saveTransactions(txns) {

    for (let tx of txns) {

        delete tx._id;

        if (!tx.data) continue;
        let instance;
        tx.blockHeight = tx.data.blockHeight;
        tx.poolOwner = poolOwner;
        if (tx.txid) {
            instance = await Transaction.findOne({ txid: tx.txid, poolOwner });
        }

        if (tx.type == 254) {
            instance = await Transaction.findOne({ type: 254, blockHash: tx.blockHash, poolOwner });
        }

        if (!instance) {
            instance = new Transaction(tx);
            if (tx.type == 254) instance.isProcess = false;
            instance.save();
        }
    }
}


async function updateWorkers() {

    let leaseTxns = await Transaction.find({ type: 2, poolOwner });

    await Worker.updateMany({ poolOwner }, { hashrate: 0 });

    console.log('update workers');

    for (let tx of leaseTxns) {

        let address = tx.from, instance;

        if (address == poolOwner) continue;

        instance = await Worker.findOne({ address, poolOwner });

        if (!instance) {
            instance = new Worker({
                address,
                hashrate: tx.data.amount,
                payouts: 0,
                poolOwner
            });
        } else {
            instance.hashrate += tx.data.amount;
        }

        await instance.save();
    }

    let cancelLeaseTxns = await Transaction.find({ type: 3, poolOwner });

    for (let tx of cancelLeaseTxns) {

        let address = tx.from, instance;

        if (address == poolOwner) continue;

        instance = await Worker.findOne({ address, poolOwner });

        if (!instance) {
            instance = new Worker({
                address,
                hashrate: tx.data.amount,
                payouts: 0
            });
        } else {
            instance.hashrate -= tx.data.amount;
        }

        await instance.save();
    }

}

async function run() {
    await initTransactions();
    require('../tools/payouts');
}
run();

