
// fix missing payment
// 


global.config = require('../config');
require('../lib/mongo');
const API = require('../lib/api');
const explorerAPI = require('../lib/explorerapi');

async function processPayment() {

    let startHeight = config.startHeight;
    let overview = await explorerAPI.getOverview();
    let currentHeight = overview.height;
    let queryHeight = currentHeight - config.pos_confirmations;
    let heightQuery = { $gt: startHeight, $lt: queryHeight }
    let payouts = await Payout.find({ isPayout: false, blockHeight: heightQuery, shares: { $gt: 0 }, amount: { $gt: 0.001 } });

    console.log(`missing payment length is ${payouts.length}`)

    let pay = {}, messages = {};

    for (let payout of payouts) {

        if (parseInt(payout.amount) == 0) continue;
        if (payout.account == config.poolOwner) continue;

        let msg = payout.blockHeight + ':' + (payout.amount / 1e8).toFixed(3);
        if (!pay[payout.account]) {

            pay[payout.account] = payout.amount / 1e8;
            messages[payout.account] = msg;
        } else {
            pay[payout.account] += payout.amount / 1e8;
            messages[payout.account] += ',' + msg;
        }
    }

    console.log(' dont interrupt pay program...');
    console.log(pay, messages);
    for (let account in pay) {

        let amount = pay[account];
        let msg = 'PoS Reward Height:' + messages[account];

        txid = await payIt(account, msg, amount * 1e8);

        if (txid && txid.length == 64) {
            await Payout.updateMany({ isPayout: false, account, blockHeight: heightQuery, shares: { $gt: 0 }, amount: { $gt: 0.001 } }, { isPayout: true, txid });
            console.log(`${account} pay success, payment txid is ${txid}`)
        } else {
            paySuccess = false;
        }
    }
}

processPayment();


async function payIt(account, msg, amount) {

    let postdata = {
        mnemonic: mnemonic,
        amount: parseInt(amount),
        fee: 100000,
        to: account,
        message: msg,
        encrypted: 0
    };
    let txid = await API.sendMoney(postdata);
    console.log(`${account}, ${amount}, ${msg}\n${txid}`);

    return txid;
}
