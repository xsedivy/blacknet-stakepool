


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');

router.get('/', async (ctx, next) => {


    await ctx.render('index', { poolOwner: config.poolOwner, fee: config.fee });
});

router.get('/status', async (ctx, next) => {


    await ctx.render('status', { poolOwner: config.poolOwner, status: true, fee: config.fee });
});
router.get('/team_up', async (ctx, next) => {


    await ctx.render('team_up', { poolOwner: config.poolOwner, team_up: true, fee: config.fee });
});


router.get('/recent_blocks', async (ctx, next) => {


    await ctx.render('recent_blocks', { recent: true });
});

router.get('/recent_payouts', async (ctx, next) => {


    await ctx.render('recent_payouts', { payouts: true });
});


router.get('/account/:account', async (ctx, next) => {

    let account = ctx.params.account;
    let payouts = await Payout.count({ account: account, shares: { $gt: 0 } });

    let t = await Payout.aggregate(
        [{ $match: { account } },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );

    let p = await Payout.aggregate(
        [{ $match: { account, isPayout: true } },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );

    let paid = 0, total_reward = 0;

    if (p[0]) paid = (p[0].total / 1e8).toFixed(8);
    if (t[0]) total_reward = (t[0].total / 1e8).toFixed(8);

    await ctx.render('account', { account, payouts, total_reward, paid });
});
router.get('/leasers', async (ctx, next) => {

    await ctx.render('leasers', { leasers: true });
});

router.get('/payouts/:blockheight', async (ctx, next) => {

    await ctx.render('payouts', {});
});

module.exports = router;

