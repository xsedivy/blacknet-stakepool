


const Router = require('koa-router');
const router = new Router({
    prefix: '/api'
});

var request = require('request');
const API = require('../lib/api');
const explorerAPI = require('../lib/explorerapi');



router.get('/website_data', async (ctx, next) => {

    let workers = await Worker.find({ poolOwner }).sort({ hashrate: 'desc' });

    let data = {};
    let hashrate = 0, number = 0;
    for (let worker of workers) {

        hashrate += worker.hashrate;
        if (worker.hashrate > 1e8) number++;
    }

    let block = await Transaction.findOne({ type: 254, poolOwner }).sort({ "data.blockHeight": -1 });

    data.hashrate = hashrate / 1e8;
    data.number = number;
    data.blockHeight = block.blockHeight;
    data.blockHash = block.blockHash;

    data.time = block.time + '000';
    data.poolOwner = poolOwner;

    let network = await explorerAPI.getNetworkHash();

    data.network = network;
    data.blocksfound = await Transaction.countDocuments({ type: 254, poolOwner });

    ctx.body = data;
});

router.get('/workers', async (ctx, next) => {

    let workers = await Worker.find({ poolOwner }).lean();
    let network = await explorerAPI.getNetworkHash();
    for (let w of workers) {

        // let status = await getPayoutStatus(w.address);
        // w.paid = status.paid;
        // w.total_reward = status.total_reward;
        // w.payouts = await Payout.countDocuments({ account: w.address , shares: {$gt: 0}});
        w.hashrate = parseInt(w.hashrate / 1e8);
        w.est = ((w.hashrate * 1e8 / network.networkhashrate) * 1440 * 20.48).toFixed(2);
    }
    ctx.body = workers;
});


router.get('/worker/:address', async (ctx, next) => {
    let address = ctx.params.address;

    let worker = await Worker.findOne({ poolOwner, address }).lean();

    if (!worker) return ctx.body = {
        paid: 0,
        total_reward: 0,
        payouts: 0,
        hashrate: 0
    };

    let status = await getPayoutStatus(worker.address);
    worker.paid = parseFloat(status.paid);
    worker.total_reward = parseFloat(status.total_reward);
    worker.payouts = status.payouts;
    worker.hashrate = parseInt(worker.hashrate / 1e8)
    ctx.body = worker;
});

router.get('/recent_blocks', async (ctx, next) => {

    let overview = await explorerAPI.getOverview();
    if (!overview) overview = {};
    let txns = await Transaction.find({ type: 254, poolOwner }).limit(100).sort({ "data.blockHeight": -1 });
    ctx.body = {
        txns,
        overview
    };
});

router.get('/recent_payouts', async (ctx, next) => {

    let url = `https://www.blnscan.io/api/account/txns/${poolOwner}?type=0`;
    ctx.body = API.simpleRequest(url);
});

router.get('/payouts/:blockHeight', async (ctx, next) => {
    let blockHeight = ctx.params.blockHeight;
    let list = await Payout.find({ blockHeight, poolOwner, shares: { $gt: 0 } });

    list = list.map((i) => {
        i.shares = parseInt(i.shares / 1e8);
        return i;
    })
    ctx.body = list;
});

router.get('/account/:account', async (ctx, next) => {

    let payments = [], page = ctx.query.page || 1;
    let account = ctx.params.account.toLowerCase(), query = { account, poolOwner, isPayout: true, shares: { $gt: 0 } };

    payments = await Payout.find(query).skip((page - 1) * 500).limit(500).sort({ blockHeight: 'desc' }).lean();

    ctx.body = payments;
});



module.exports = router;


let status = {};

async function getPayoutStatus(account) {

    if (status[account] && Date.now() - status[account].now < 60 * 1000) {
        return status[account];
    }


    let paid = 0, total_reward = 0;
    let t = await Payout.aggregate(
        [{ $match: { account } },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );

    let p = await Payout.aggregate(
        [{ $match: { isPayout: true } }, { $match: { account } },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );
    if (p[0] && t[0]) {

        paid = (p[0].total / 1e8).toFixed(8);
        total_reward = (t[0].total / 1e8).toFixed(8);
    }

    let payouts = await Payout.countDocuments({ account: account, shares: { $gt: 0 } });

    status[account] = { paid, total_reward, payouts };
    status[account].now = Date.now();
    return { paid, total_reward, payouts }
}
