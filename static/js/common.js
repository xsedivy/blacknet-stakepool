/*
 * Copyright (c) 2018-2019 Blacknet Team
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

void function () {

    const Blacknet = {};
    const blockListEl = $('#block-list'), apiVersion = "/api", body = $("body");;
    const progressStats = $('.progress-stats, .progress-stats-text');
    const dialogPassword = $('.dialog.password'), mask = $('.mask');
    const account = localStorage.account;
    const dialogAccount = $('.dialog.account');


    
    Blacknet.toBLNString = function(number){
        return new BigNumber(number).dividedBy(1e8).toFixed(8) + ' BLN';
    };



    Blacknet.renderStatus = async function () {

        // let overview = await Blacknet.getPromise('/overview', 'json');
        // let network = $('.network');

        // network.find('.supply').html(new BigNumber(overview.supply).dividedBy(1e8).toFixed(0));
        // network.find('.connections').text(overview.outgoing + overview.incoming);
    };

    Blacknet.get = function (url, callback) {

        return $.get(apiVersion + url, callback);
    };

    Blacknet.getPromise = function (url, type) {

        return type == 'json' ? $.getJSON(apiVersion + url) : $.get(apiVersion + url);
    };
    Blacknet.post = function (url, callback, type) {
        return $.post(apiVersion + url, {}, callback, type);
    };

    Blacknet.postPromise = function (url) {
        return $.post(apiVersion + url, {});
    };

    Blacknet.wait = function (timeout) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve();
            }, timeout);
        });
    };

    Blacknet.unix_to_local_time = function (unix_timestamp) {

        let date = new Date(unix_timestamp * 1000);
        let hours = "0" + date.getHours();
        let minutes = "0" + date.getMinutes();
        let seconds = "0" + date.getSeconds();
        let day = date.getDate();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;

        return year + "-" + ('0' + month).substr(-2) + "-" +
            ('0' + day).substr(-2) + " " + hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    }


    Blacknet.ready = async function (callback) {

        let lang = navigator.language || navigator.userLanguage;
        if (lang.indexOf('zh') !== -1) {
            i18n({ locale: 'zh_CN' });
        } else if (lang.indexOf('ja') !== -1) {
            i18n({ locale: 'ja' });
        } else if (lang.indexOf('sk') !== -1) {
            i18n({ locale: 'sk' });
     // } else if (lang.indexOf('ru') !== -1) {
     //     i18n({ locale: 'ru' });
        } else if (lang.indexOf('de') !== -1) {
            i18n({ locale: 'de' });
        }else{
            await Blacknet.wait(300);
            $('.dialog').hide();
        }
        

    };
    Blacknet.renderStatus();

    $('#filter_pos').on('change', function(){
        
        location.href=location.pathname + '?page=1&filter_pos_generated=' + (this.checked ? 1 : 0);
    });

    window.Blacknet = Blacknet;
    Blacknet.ready();
}();
