# blacknet-stakepool

Stakepool for [Blacknet](https://blacknet.ninja/)

website: https://www.blnpool.io

# getstarted ( ubuntu)

## 0 
```
apt-get install -y git
```
## 1. install mongodb
```
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-4.0.11.tgz
tar zvxf mongodb-linux-x86_64-4.0.11.tgz
cd mongodb-linux-x86_64-4.0.11
cp -r * /usr/local
mkdir -p /data/db
nohup mongod --dbpath=/data/db --bind_ip=127.0.0.1 &
```
## 2. install nodejs
```
wget https://nodejs.org/dist/v12.13.1/node-v12.13.1-linux-x64.tar.xz
tar -vxf node-v12.13.1-linux-x64.tar.xz
cd node-v12.13.1-linux-x64
cp -r * /usr/local
```
## 3 clone source code

```
git clone https://gitlab.com/blacknet-ninja/blacknet-stakepool
cd blacknet-stakepool
npm i
```

## 4. edit config/index.json

```

{
    "alias": "Stake Pool",
    "pos_confirmations": 10,
    "pos_mature_blocks": 1350, 
    "fee": 4, // fee 4%
    "poolOwner": "blacknet1......",// pool address
    "startHeight": 447000, // the block height of pool starting
    "api": "http://www.blacknetapi.com:8283/api/v2" // Blacknet core application host
}


```

## 5. start webserver

```
node server.js
```

## 6. start pay rewards

```
# need input the Mnemonic Code of pool address
node start_pay.js

```

## 7. browser 
```
http://your_server_ip:4000
```

# tips

## resetup pool

```
# clear all data and return step #5 and #6 
node tools/cleardata.js

```

