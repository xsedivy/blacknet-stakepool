

const path = require('path');
const Koa = require('koa');
const serve = require('koa-static');
const views = require('koa-views');
const app = new Koa();
// logger
require('./lib/mongo');

global.config = require('./config/index.json');
global.poolOwner = config.poolOwner;

require('./lib/app');

const router = require('./route');
const apirouter = require('./route/api');

app.use(views(path.join(__dirname, 'views'), {
  options: {
    settings: {
      views: path.join(__dirname, 'views')
    }
  },
  extension: 'njk',
  map: {
    njk: 'nunjucks'
  }
}))
app.use(async (ctx, next) => {
  await next();
  if (ctx.status == 404) {
    await ctx.render('404');
  }
  if (ctx.status >= 500) {
    await ctx.render('500');
  }
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});
app.use(serve(__dirname + '/static'));

app.use(router.routes())
  .use(apirouter.routes()).use(router.allowedMethods());

app.listen(4000);

